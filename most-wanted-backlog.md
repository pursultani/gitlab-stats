# Most wanted backlogged issues

| Issue        | # Votes   |
| ------------ | --------- |
| [Report number of lines per language in repository charts](https://gitlab.com/gitlab-org/gitlab/-/issues/17800) | 461 |
| [Visual UML diagram editor](https://gitlab.com/gitlab-org/gitlab/-/issues/15932) | 324 |
| [Runner Priority](https://gitlab.com/gitlab-org/gitlab/-/issues/14976) | 291 |
| [Group-Level Snippets](https://gitlab.com/gitlab-org/gitlab/-/issues/15958) | 272 |
| [List of all subscribed issues](https://gitlab.com/gitlab-org/gitlab/-/issues/14972) | 255 |
| [Implement cross-server (federated) merge requests](https://gitlab.com/gitlab-org/gitlab/-/issues/14116) | 251 |
| [Markdown pages - export to PDF](https://gitlab.com/gitlab-org/gitlab/-/issues/13932) | 249 |
| [Automated tagging after merge request](https://gitlab.com/gitlab-org/gitlab/-/issues/15962) | 234 |
| [Multiple version Pages support](https://gitlab.com/gitlab-org/gitlab/-/issues/16208) | 221 |
| [Desktop Push Web Notifications](https://gitlab.com/gitlab-org/gitlab/-/issues/17232) | 219 |
| [Allow API project access with ci_job_token for internal project or public project with member only access to repository or private project](https://gitlab.com/gitlab-org/gitlab/-/issues/17511) | 216 |
| [Change default target branch for merge requests](https://gitlab.com/gitlab-org/gitlab/-/issues/17909) | 216 |
| [Configure label to be removed when issue is closed.](https://gitlab.com/gitlab-org/gitlab/-/issues/17461) | 198 |
| [Access issue custom fields from project level](https://gitlab.com/gitlab-org/gitlab/-/issues/1906) | 185 |
| [Allow `needs:` (DAG) to refer to a job in the same stage](https://gitlab.com/gitlab-org/gitlab/-/issues/30632) | 185 |
| [Add control over images with Markdown](https://gitlab.com/gitlab-org/gitlab/-/issues/15697) | 180 |
| [Group for access to issue tracker, no license necessary](https://gitlab.com/gitlab-org/gitlab/-/issues/2105) | 171 |
| [Make it possible to edit wiki through CI](https://gitlab.com/gitlab-org/gitlab/-/issues/16261) | 169 |
| [Add code comments in file (blob) view](https://gitlab.com/gitlab-org/gitlab/-/issues/15470) | 167 |
| [Download (clone) statistics](https://gitlab.com/gitlab-org/gitlab/-/issues/15807) | 166 |
| [Draw.io integration with wiki](https://gitlab.com/gitlab-org/gitlab/-/issues/20305) | 159 |
| [Solution for mass-assigning deploy keys](https://gitlab.com/gitlab-org/gitlab/-/issues/14106) | 152 |
| [Automatically generate a changelog based on merge request](https://gitlab.com/gitlab-org/gitlab/-/issues/15148) | 150 |
| [Add comments on wiki pages](https://gitlab.com/gitlab-org/gitlab/-/issues/19591) | 146 |
| [Add slack direct messages as a notification option](https://gitlab.com/gitlab-org/gitlab/-/issues/17958) | 145 |
| ["Pipeline doesn't succeed when manual jobs using new DAG dependency ""needs:"" are waiting for other ""when: manual"" jobs to succeed"](https://gitlab.com/gitlab-org/gitlab/-/issues/31264) | 144 |
| [Create an option to archive/unarchive Project Groups](https://gitlab.com/gitlab-org/gitlab/-/issues/15967) | 139 |
| [GitLab Pages without DNS wildcard - MVC](https://gitlab.com/gitlab-org/gitlab/-/issues/17584) | 138 |
| [Add configurable pipeline priority](https://gitlab.com/gitlab-org/gitlab/-/issues/14559) | 136 |
| [Add branch references to GFM](https://gitlab.com/gitlab-org/gitlab/-/issues/17084) | 130 |
| [Real-time boards lists - Backend](https://gitlab.com/gitlab-org/gitlab/-/issues/16020) | 128 |
| [Make it possible to identify a new/empty branch with workflow:rules](https://gitlab.com/gitlab-org/gitlab/-/issues/15170) | 127 |
| [gitlab-ci.yaml syntax for merging YAML arrays (i.e., script or after_script, rules, etc.)](https://gitlab.com/gitlab-org/gitlab/-/issues/16376) | 125 |
| [New gitlab-ci predefined variable for job start time](https://gitlab.com/gitlab-org/gitlab/-/issues/22901) | 125 |
| [Support multiple artifacts generation per job](https://gitlab.com/gitlab-org/gitlab/-/issues/18744) | 122 |
| [Trello issues importer](https://gitlab.com/gitlab-org/gitlab/-/issues/14917) | 120 |
| [Attachment Manager](https://gitlab.com/gitlab-org/gitlab/-/issues/16229) | 120 |
| [Integrate GitLab with AWS CodePipeline](https://gitlab.com/gitlab-org/gitlab/-/issues/19082) | 120 |
| [Overview of Issue Boards Across ALL projects](https://gitlab.com/gitlab-org/gitlab/-/issues/15757) | 116 |
| [Project and branch variables for markdown](https://gitlab.com/gitlab-org/gitlab/-/issues/14389) | 113 |
| [Cargo (Rust) Package Manager MVC](https://gitlab.com/gitlab-org/gitlab/-/issues/33060) | 113 |
| [Full project mirroring between GitLab instances](https://gitlab.com/gitlab-org/gitlab/-/issues/4517) | 110 |
| [Follow-up: Introduce Parameters to bulk delete API endpoint](https://gitlab.com/gitlab-org/gitlab/-/issues/14495) | 110 |
| [Allow for variable substitution in CI tag names (Dynamic Tags)](https://gitlab.com/gitlab-org/gitlab/-/issues/35742) | 110 |
| [Integrate GitLab with AWS CodeBuild](https://gitlab.com/gitlab-org/gitlab/-/issues/19081) | 107 |
| [create ci-extended-job-token in the ci-job-info](https://gitlab.com/gitlab-org/gitlab/-/issues/20416) | 107 |
| [Gitlab CI - Run job on all runners that have the specific tag](https://gitlab.com/gitlab-org/gitlab/-/issues/16474) | 104 |
| [project topics (was tags) should be usable in search](https://gitlab.com/gitlab-org/gitlab/-/issues/14220) | 102 |
| [Default target branch in Merge Request is always a repository I have no access to](https://gitlab.com/gitlab-org/gitlab/-/issues/14522) | 102 |
| [Remember Project Sort Order in Dashboard](https://gitlab.com/gitlab-org/gitlab/-/issues/18871) | 102 |
| [Enable Cleanup policy for tags for all existing projects on both GitLab.com and self-managed instances](https://gitlab.com/gitlab-org/gitlab/-/issues/196124) | 102 |
| [Optional manual action becomes blocking when it is started](https://gitlab.com/gitlab-org/gitlab/-/issues/20237) | 101 |
| [Profile Timezone](https://gitlab.com/gitlab-org/gitlab/-/issues/15137) | 100 |
| [Make it possible to view rendered html](https://gitlab.com/gitlab-org/gitlab/-/issues/18248) | 98 |
| [Rebase and merge with one click](https://gitlab.com/gitlab-org/gitlab/-/issues/895) | 97 |
| [set artifacts visibility independent of the porject or group visibility, i.e. Private, Internal, Public.](https://gitlab.com/gitlab-org/gitlab/-/issues/17544) | 97 
| [Block merge request with a negative approval signal](https://gitlab.com/gitlab-org/gitlab/-/issues/761) | 96 |
| [README or "About" for groups](https://gitlab.com/gitlab-org/gitlab/-/issues/15041) | 96 |
| [Subissues on issue board](https://gitlab.com/gitlab-org/gitlab/-/issues/2530) | 95 |
| [Add support for mailmap configuration](https://gitlab.com/gitlab-org/gitlab/-/issues/14909) | 92 |
| [Filter by (and board config) Project on Group Issue Board](https://gitlab.com/gitlab-org/gitlab/-/issues/4389) | 91 |
| [Secret personal snippets](https://gitlab.com/gitlab-org/gitlab/-/issues/14201) | 91 |
| [Option to send Pipeline notifications to Commit Author or specific user](https://gitlab.com/gitlab-org/gitlab/-/issues/20281) | 90 |
| [Remove branch-related images from the registry when the branch is merged](https://gitlab.com/gitlab-org/gitlab/-/issues/26983) | 90 |
| [Identify docker image as build artifact for specific pipeline](https://gitlab.com/gitlab-org/gitlab/-/issues/18264) | 88 |
| [Identify docker image as build artifact for specific pipeline](https://gitlab.com/gitlab-org/gitlab/-/issues/18264) | 88 |
| [Define pull policy for image in gitlab-ci.yml](https://gitlab.com/gitlab-org/gitlab/-/issues/21619) | 86 |
| [Allow .gitlab/ folders to have default template for (issue merge request) templates instead of settings](https://gitlab.com/gitlab-org/gitlab/-/issues/2557) | 85 |
| [Disable CI Builds for WIP MRs](https://gitlab.com/gitlab-org/gitlab/-/issues/15065) | 85 |
| [Sync project labels with admin area's labels](https://gitlab.com/gitlab-org/gitlab/-/issues/14975) | 81 |
| [Create bridge for Chat platforms](https://gitlab.com/gitlab-org/gitlab/-/issues/16711) | 81 |
| [Create Personal issue boards](https://gitlab.com/gitlab-org/gitlab/-/issues/18073) | 81 |
| [Retry manual job with different variables](https://gitlab.com/gitlab-org/gitlab/-/issues/32712) | 81 |
| [Specify Dockerfile to build image for use in CI/CD](https://gitlab.com/gitlab-org/gitlab/-/issues/22801) | 80 |
| [Markdown support for cross-project links to files](https://gitlab.com/gitlab-org/gitlab/-/issues/17627) | 79 |
| [Usage of variables in only: changes: paths](https://gitlab.com/gitlab-org/gitlab/-/issues/8177) | 78 |
| [Edit commit messages when merging merge request](https://gitlab.com/gitlab-org/gitlab/-/issues/2551) | 77 |
| [Add "estimated time" as an option to quantify milestone progress via burndown charts and the sidebar](https://gitlab.com/gitlab-org/gitlab/-/issues/4004) | 77 |
| [Create pipeline schedules for tag targets](https://gitlab.com/gitlab-org/gitlab/-/issues/23292) | 77 |
| [Enable automatic issue closing for non default branches](https://gitlab.com/gitlab-org/gitlab/-/issues/14289) | 76 |
| [Feature request: Global and per-user CI variables](https://gitlab.com/gitlab-org/gitlab/-/issues/15815) | 76 |
| [Confirmation for manual CI actions](https://gitlab.com/gitlab-org/gitlab/-/issues/18906) | 74 |
| [Reflect my specified timezone throughout GitLab](https://gitlab.com/gitlab-org/gitlab/-/issues/19496) | 74 |
| [CI: Caching outside repository seems impossible](https://gitlab.com/gitlab-org/gitlab/-/issues/14151) | 73 |
| [Helm Charts Package Manager MVC](https://gitlab.com/gitlab-org/gitlab/-/issues/18997) | 73 |
| [Configure default branch name for branches created from merge request](https://gitlab.com/gitlab-org/gitlab/-/issues/21243) | 73 |
| [Configure default assignee for issues](https://gitlab.com/gitlab-org/gitlab/-/issues/14585) | 72 |
| [Display CI job backlog for each runner](https://gitlab.com/gitlab-org/gitlab/-/issues/15291) | 72 |
| [Repository -> Commits: see commits of all branches](https://gitlab.com/gitlab-org/gitlab/-/issues/19703) | 72 |
| [Support ignoring commits in blame (blame.ignoreRevsFile config option)](https://gitlab.com/gitlab-org/gitlab/-/issues/31423) | 72 |
| [Allow CI/CD mirroring to handle forks on GitHub SCM](https://gitlab.com/gitlab-org/gitlab/-/issues/5667) | 71 |
| [Allow users to set Wiki as default project view](https://gitlab.com/gitlab-org/gitlab/-/issues/14995) | 71 |
| [Option to Expand All by default in diffs](https://gitlab.com/gitlab-org/gitlab/-/issues/17774) | 71 |
| [Transfer project from one user to another](https://gitlab.com/gitlab-org/gitlab/-/issues/14502) | 70 |
| [Support relative links from a wiki page to files in the repository](https://gitlab.com/gitlab-org/gitlab/-/issues/15602) | 70 |
| [Project based snippets should follow project access permissions](https://gitlab.com/gitlab-org/gitlab/-/issues/16256) | 70 |
| [Change the default initial branch name for new projects on GitLab](https://gitlab.com/gitlab-org/gitlab/-/issues/221164) | 70 |
| [Concurrent runners per executor but only one per Project](https://gitlab.com/gitlab-org/gitlab/-/issues/15325) | 69 |
| [Cannot use $ character in build variables](https://gitlab.com/gitlab-org/gitlab/-/issues/17069) | 69 |
